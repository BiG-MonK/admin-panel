HTMLIFrameElement.prototype.load = function (url, callback) {
    const iframe = this;
    try {
        iframe.src = url + "?rnd=" + Math.random().toString().substring(2);
    } catch (error) {
        if (!callback) {
            return new Promise((resolve, reject) => {
                reject(error);
            });
        } else {
            callback(error);
        }
    }
    
    const maxTime = 60000;
    const interval = 200;
    let timerCount = 0;

    let attempt = function(f_1, f_2){
        const timer = setInterval(function() {
            if (!iframe) return clearInterval(timer);
            timerCount++;
            if (iframe.contentDocument && iframe.contentDocument.readyState === "complete") {
                clearInterval(timer);
                f_1();
            } else if (timerCount * interval > maxTime) {
                f_2(new Error("Iframe load fail!"));
            }
        }, interval);
    }

    if (!callback) {
        return new Promise((resolve, reject) => {
            attempt(resolve, reject);
        });
    } else {
        attempt(callback, callback);
    }
};